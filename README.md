# WP SDV Flyer

The `SDV Flyer` plugin.

## New release

1. Set the new version **X.Y** in the `sdv-flyer.php` file comments section.
2. Commit and push.
3. Add a new repository tag **X.Y** without writing release notes.
4. Gitlab CI would create a release asset.
5. Once finished (see pipeline), update the stable version in `readme.txt` and commit/push.
