��          <      \       p      q   	   �      �   g  �   H     	   \     f                   Provides the GFF SDV Flyer. SDV Flyer https://www.gff.ch/ Project-Id-Version: SDV Flyer 1.0
Report-Msgid-Bugs-To: adrian.suter@gff.ch
PO-Revision-Date: 2023-03-29 16:14+0200
Last-Translator: Adrian Suter <adrian.suter@gff.ch>
Language-Team: 
Language: de_DE
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 3.2.2
 GFF SDV Flyer stellt die Flyer-Funktion (Drogistenstern) zur Verfügung. SDV Flyer https://www.gff.ch/ 