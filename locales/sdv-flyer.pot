# Copyright (C) 2023 GFF Integrative Kommunikation GmbH
# This file is distributed under the Proprietary.
msgid ""
msgstr ""
"Project-Id-Version: SDV Flyer 1.0\n"
"Report-Msgid-Bugs-To: adrian.suter@gff.ch\n"
"POT-Creation-Date: 2023-03-29 14:08:21+00:00\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"PO-Revision-Date: 2023-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"X-Generator: grunt-wp-i18n 1.0.3\n"

#. Plugin Name of the plugin/theme
msgid "SDV Flyer"
msgstr ""

#. Description of the plugin/theme
msgid "Provides the GFF SDV Flyer."
msgstr ""

#. Author URI of the plugin/theme
msgid "https://www.gff.ch/"
msgstr ""