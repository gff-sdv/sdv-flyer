(function (sdv_flyer) {
  const doc = document;

  function domReady (fn) {
    doc.addEventListener('DOMContentLoaded', fn);
    if (doc.readyState === 'interactive' || doc.readyState === 'complete') {
      fn();
    }
  }

  domReady(() => {
    const target_url = sdv_flyer['target_url'];
    const image_path = sdv_flyer['image_url'];

    /**
     * Gets the offsets of an element.
     *
     * @param {HTMLElement} el
     * @returns {{top: Number, left: Number, bottom: Number, right: Number}}
     */
    const get_offset = (el) => {
      const rect = el.getBoundingClientRect();
      return {
        left: rect.left + window.scrollX,
        right: rect.right + window.scrollX,
        top: rect.top + window.scrollY,
        bottom: rect.bottom + window.scrollY
      };
    };

    /**
     * Gets the target x-coordinate.
     * @returns {Number}
     */
    const get_target_x = () => {
      const el = doc.querySelector('.site-branding');
      return get_offset(el).right;
    };

    /**
     * Gets the target y-coordinate.
     * @returns {Number}
     */
    const get_target_y = () => {
      const g = doc.querySelector('.site-description'); //getElementById('cb-row--header-bottom');
      return get_offset(g).bottom;
    };

    ///
    // Get the body element.
    ///
    const body_element = doc.querySelector('body');

    ///
    // Build the container element.
    ///
    const image_element = doc.createElement('img');
    image_element.src = image_path;
    image_element.alt = '';

    const a_element = doc.createElement('a');
    a_element.setAttribute('href', target_url);
    a_element.setAttribute('target', '_blank');
    a_element.appendChild(image_element);

    const container_element = doc.createElement('div');
    container_element.classList.add('sdv-flyer-container', 'hide-on-mobile', 'hide-on-tablet');
    container_element.appendChild(a_element);

    ///
    // Get the xy-coordinates the animation should begin at.
    ///
    let x = body_element.clientWidth;
    let y = get_target_y();

    let interval_duration = 1 / 10; // seconds
    let gravity = 400; // pixels per second

    let floor = get_target_y();
    let target_x = get_target_x();
    let target_y = get_target_y();

    let requested_distance_x = (target_x - x) / 2;

    let theta = 60 * Math.PI / 180;
    let speed = Math.sqrt(requested_distance_x * gravity / Math.sin(2 * theta));

    let speed_x = speed * Math.cos(theta); // pixels per second
    let speed_y = -speed * Math.sin(theta); // pixels per second

    container_element.style.transition = 'transform ' + interval_duration + 's linear';
    container_element.style.transform = 'translate(' + x + 'px, ' + y + 'px)';

    ///
    // Append the container element to the wrapper element and append the latter to the body element.
    ///
    const wrapper_element = doc.createElement('div');
    wrapper_element.classList.add('sdv-flyer-wrapper');
    wrapper_element.appendChild(container_element);
    body_element.appendChild(wrapper_element);

    setInterval(() => {
      floor = get_target_y();

      let target_changed = false;
      if (get_target_x() !== target_x) {
        target_x = get_target_x();
        target_changed = true;
      }
      if (get_target_y() !== target_y) {
        target_y = get_target_y();
        target_changed = true;
      }

      if (!isNaN(speed_x)) {
        x = x + speed_x * interval_duration;
      }

      if (!isNaN(speed_y)) {
        y = y + speed_y * interval_duration + 0.5 * gravity * Math.pow(interval_duration, 2);
        speed_y = speed_y + gravity * interval_duration;
      }

      // let jump_speed = 10;
      if (y >= floor || target_changed) {
        requested_distance_x = (target_x - x) / 2;

        if (requested_distance_x >= 0) {
          theta = 60 * Math.PI / 180;
        } else {
          theta = 120 * Math.PI / 180;
        }

        speed = Math.sqrt(requested_distance_x * gravity / Math.sin(2 * theta));
        if (speed < 10) {
          speed = 0;
        }

        speed_x = speed * Math.cos(theta); // pixels per second
        speed_y = -speed * Math.sin(theta); // pixels per second
      }

      if (y >= floor) {
        y = floor;
      }

      container_element.style.transform = 'translate(' + x + 'px, ' + y + 'px)';
    }, interval_duration * 1000);

    let h = 190;

    image_element.style.height = h + 'px';
    a_element.style.left = '60px';
    a_element.style.top = '-' + h + 'px';
  });
})(sdv_flyer);
