module.exports = function (grunt) {
  grunt.loadNpmTasks('grunt-wp-i18n');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-compress');
  grunt.loadNpmTasks('grunt-contrib-watch');

  const jsView = [
    'js/sdv_flyer.js'
  ];

  // noinspection JSUnresolvedFunction
  grunt.initConfig({
    makepot: {
      main: {
        options: {
          cwd: '../',
          mainFile: 'sdv-flyer.php',
          domainPath: 'locales',
          potHeaders: {
            'Report-Msgid-Bugs-To': 'adrian.suter@gff.ch'
          },
          exclude: ['_dev'],
          processPot: function (pot) {
            delete pot['translations']['']['https://www.gff.ch'];
            delete pot['translations']['']['GFF Integrative Kommunikation GmbH'];
            delete pot['translations']['']['Settings'];

            return pot;
          }
        }
      }
    }
    ,
    uglify: {
      options: {
        mangle: true,
        compress: true
      },
      jsView: {files: {'../scripts/sdv_flyer.js': jsView}}
    }
    ,
    watch: {
      jsView: {files: jsView, tasks: ['uglify:jsView']},
    }
    ,
    compress: {
      main: {
        options: {
          archive: function () {
            return '../sdv-flyer.zip';
          },
          mode: 'zip'
        },
        files: [
          {
            expand: true,
            cwd: '../',
            src: [
              '**',
              '!_dev/**',
              '!assets/*.xcf',
              '!.gitignore',
              '!.gitlab-ci.yml',
              '!README.md',
            ],
            dest: 'sdv-flyer/'
          }
        ]
      }
    }
  });

  // Register the tasks.
  grunt.registerTask('archive', ['compress']);
};
