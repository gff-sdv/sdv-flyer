<?php

/*
Plugin Name: SDV Flyer
Description: Provides the GFF SDV Flyer.
Version: 1.13
Author: GFF Integrative Kommunikation GmbH
Author URI: https://www.gff.ch/
License: Proprietary
Text Domain: sdv-flyer
Domain Path: /locales
*/

declare( strict_types=1 );

const SDV_FLYER_JS_VERSION = '1.0';

const SDV_FLYER_CSS_VERSION = '1.0';

add_action( 'init', function () {
	wp_register_script( 'sdv-flyer', plugins_url( 'scripts/sdv_flyer.js', __FILE__ ), [], SDV_FLYER_JS_VERSION );
	wp_register_style( 'sdv-flyer', plugins_url( 'styles/sdv_flyer.css', __FILE__ ), [], SDV_FLYER_CSS_VERSION );
} );

add_action( 'wp_enqueue_scripts', function () {
	if ( is_front_page() ) {
		$current_language = apply_filters( 'wpml_current_language', null );

		// TODO [AS] Use `options` to set the target urls and then fetch the cover urls from these.
		//    Example for DS 1 2025 (fr):
		//    <meta property="og:image" content="https://image.isu.pub/250218125910-d8fa5ce4e3913b3a5e3184df6ba9b714/jpg/page_1_social_preview.jpg" />
		$target_url = 'https://issuu.com/vitagate-ag/docs/drogistenstern_1_2025';
		$image_url  = plugins_url( 'assets/2025-1-Drogistenstern-Magazin.jpg', __FILE__ );
		if ( $current_language === 'fr' ) {
			$target_url = 'https://issuu.com/vitagate-ag/docs/tribune_du_droguiste_1_2025';
			$image_url  = plugins_url( 'assets/2025-1-Tribune-du-droguiste-magazine.jpg', __FILE__ );
		}

		wp_localize_script( 'sdv-flyer', 'sdv_flyer', [
			'target_url' => $target_url,
			'image_url'  => $image_url,
		] );

		wp_enqueue_script( 'sdv-flyer' );
		wp_enqueue_style( 'sdv-flyer' );
	}
} );

////////////////////// Update Checker //////////////////////

add_action( 'plugins_loaded', function () {
	if ( ! class_exists( Puc_v4_Factory::class ) ) {
		return;
	}

	$group = 'gff-sdv';
	$slug  = 'sdv-flyer';

	$updateChecker = Puc_v4_Factory::buildUpdateChecker(
		'https://gitlab.com/' . $group . '/' . $slug . '/',
		__FILE__,
		basename( __FILE__, '.php' )
	);

	/** @var Puc_v4p11_Vcs_GitLabApi $vcsApi */
	$vcsApi = $updateChecker->getVcsApi();
	$vcsApi->enableReleasePackages();
} );
