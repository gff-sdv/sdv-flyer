=== SDV Flyer ===
Contributors: masterida
Donate link: https://www.gff.ch/
Requires at least: 6.0
Tested up to: 6.7
Requires PHP: 7.4
Stable tag: 1.13
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

Provides the GFF SDV Flyer.

== Description ==

This plugin provides the GFF SDV Flyer.

== Frequently Asked Questions ==

None.

== Upgrade Notice ==

Please upgrade as soon as possible.

== Changelog ==

= 1.13 =
* Update the flyer to 2025-1.

= 1.12 =
* Update the flyer to 2024-6.

= 1.11 =
* Update the flyer to 2024-5.

= 1.10 =
* Update the flyer to 2024-4.

= 1.9 =
* Update the flyer to 2024-3.

= 1.8 =
* Update the flyer to 2024-2.

= 1.7 =
* Update the flyer to 2024-1.

= 1.6 =
* Update the flyer to 2023-6.

= 1.5 =
* Update the flyer to 2023-5.

= 1.4 =
* Update the flyer to 2023-4.

= 1.2 =
* Upgrade tested WordPress version - again.

= 1.1 =
* Upgrade tested WordPress version.

= 1.0 =
* First official version.
